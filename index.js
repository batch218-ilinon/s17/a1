// console.log("Hello World");

/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
    function getUserInfos(){
        let fullName = prompt("Enter your fullname:");
        let age = prompt("Enter your age:");
        let location = prompt("Enter your location:");

        console.log("Hello, " + fullName);
        console.log("You are " + age + " years old.");
        console.log("You live in " + location);
    };

    getUserInfos();

/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
    function printFavoriteBands(){
        console.log("1. Bruno Mars");
        console.log("2. Eminem");
        console.log("3. Michael Jackson");
        console.log("4. The Beatles");
        console.log("5. Metallica");
    };

    printFavoriteBands();


/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
    function printFavoriteMovies(){
        console.log("1. 3 Idiots\nRotten Tomatoes Rating: 100%");
        console.log("2. Ocean's Eleven\nRotten Tomatoes Rating: 83%");
        console.log("3. Sherlock Holmes\nRotten Tomatoes Rating: 95%");
        console.log("4. The Godfather\nRotten Tomatoes Rating: 97%");
        console.log("5. The Godfather II\nRotten Tomatoes Rating: 96%");
    }

    printFavoriteMovies();
/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();


// console.log(friend1);
// console.log(friend2);